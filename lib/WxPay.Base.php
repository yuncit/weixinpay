<?php
require_once "WxPay.Exception.php";
require_once "WxPay.Config.php";

/**
 * 
 * 接口访问类，包含所有微信支付API列表的封装，类中方法为static方法，
 * 每个接口有默认超时时间（除提交被扫支付为10s，上报超时时间为1s外，其他均为6s）
 * @author widyhu
 *
 */
class WxPayBase
{
	public $errCode = 40001;
	public $errMsg = "no access";
	
	protected $appid='';
	protected $appsecret='';
	protected $mch_id='';
	protected $key='';	
	protected $cert_path='';
	
	public function __construct() {
		$args = func_get_args();
		if(count($args) > 0){
			$options = $args[0];
			$this->appid = isset($options['appid'])?$options['appid']:'';
			$this->appsecret = isset($options['appsecret'])?$options['appsecret']:'';
			$this->mch_id = isset($options['mch_id'])?$options['mch_id']:'';
			$this->key = isset($options['key'])?$options['key']:'';
			if(!empty($options['cert_path'])){
				$this->cert_path = isset($options['cert_path'])?$options['cert_path']:'';	
			}			
		}
	}
	
	public function getVersion()
	{
		return 'WXPAY 1.0.0';
	}
	
	protected function getConfig()
	{
		$options = array(
			'appid'=>$this->appid,
			'appsecret'=>$this->appsecret,
			'mch_id'=>$this->mch_id,
			'key'=>$this->key,
			'cert_path'=>$this->cert_path,
		);
		return $options;
	}	
}

