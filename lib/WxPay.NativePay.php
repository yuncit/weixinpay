<?php
require_once "WxPay.Data.php";
require_once "WxPay.Api.php";

/**
 * 
 * 刷卡支付实现类
 * @author widyhu
 *
 */
class NativePay extends WxPayDataBase
{
	/**
	 * 
	 * 生成扫描支付URL,模式一
	 * @param BizPayUrlInput $bizUrlInfo
	 */
	public function GetPrePayUrl($productId)
	{
		$biz = new WxPayBizPayUrl($this->getConfig());
		$biz->SetProduct_id($productId);
		$api = new WxpayApi($this->getConfig());
		$values = $api->bizpayurl($biz);
		$url = "weixin://wxpay/bizpayurl?" . $this->ToUrlParams($values);
		return $url;
	}	
	/**
	 * 
	 * 生成直接支付url，支付url有效期为2小时,模式二
	 * @param UnifiedOrderInput $input
	 */
	public function GetPayUrl($input)
	{
		if($input->GetTrade_type() == "NATIVE")
		{
			$api = new WxPayApi($this->getConfig());
			$result = $api->unifiedOrder($input);
			return $result;
		}
		else{
			return false;
		}
	}
}